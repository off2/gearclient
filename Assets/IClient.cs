﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void DisconnectHandler();
public delegate void ConnectHandler(bool connect);
public delegate void ReceiveServerPushHandler(int packetId, string protocolName, string response);
public delegate void ReceiveHandler(object response);


public interface IClient  {

	event DisconnectHandler OnDisconnect;
	event ConnectHandler OnConnect;
	event ReceiveServerPushHandler OnReceiveServerPush;

	void StartNetwork (string url, int port);
	void StopNetwork();
	void Send<REQ> (REQ request, ReceiveHandler receiveHandler);

	void ResetAllEventForTest ();

}
		
