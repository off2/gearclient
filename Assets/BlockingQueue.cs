﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System;
using UnityEngine;

public class BlockingQueue<T>{

	private Queue<T> queue = new Queue<T>();

	public T WaitDequeue() {
		lock (this) {

			while (true) {
				if (queue.Count == 0) 
					Monitor.Wait (this);
				else 
					return queue.Dequeue ();
				
			}
		}
	}

	public void Enqueue(T e) {
		lock (this) {
			queue.Enqueue (e);
			Monitor.PulseAll (this);
		}

	}

	public void Clear() {
		lock (this) {
			queue.Clear ();
		}
	}

//	public void PulseQueue() {
//			Monitor.Pulse (this);
//	}

}
