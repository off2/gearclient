﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DefaultResponse{
	public byte ret;
	public string ed;
}

public class JoinRequest {
	public int id;
}

public class JoinResponse : DefaultResponse{

}


public class Protocols {
	private static Hashtable protocols = new Hashtable();

	static Protocols() {
		//Add new protocols here
		protocols.Add (typeof(JoinRequest), new ProtocolInfo ("join", typeof(JoinResponse), true, 5));
		protocols.Add (typeof(TestRequest), new ProtocolInfo ("test", typeof(TestResponse), true, 5));
	}

	public static ProtocolInfo getProtocol<REQ>() {
		return (ProtocolInfo) (protocols[typeof(REQ)]);
	}
}

public class ProtocolInfo { 
	private string name;
	private bool hasResponse;
	private int timeOut; /*in sec*/
	private Type responseType;

	public string Name { get { return name; }}
	public bool HasResponse { get { return hasResponse; }}
	public int TimeOut { get { return timeOut; }}
	public Type  ResponseType{ get { return responseType; }}

	public ProtocolInfo(string name,Type type, bool hasResponse, int timeOut) {
		this.name = name;
		this.responseType = type;
		this.hasResponse = hasResponse;
		this.timeOut = timeOut;
	}

}