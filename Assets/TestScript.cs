﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class TestScript : MonoBehaviour {

	public IClient socketClient;

	private EventWaitHandle nextEvent = new AutoResetEvent(false);

	// Use this for initialization
	IEnumerator Start () {

		socketClient = ClientImpl.Instance;

		ConnectFailTest ();
		yield return StartCoroutine ("WaitNext", 0);
//
		ConnectTest ();
		yield return StartCoroutine ("WaitNext", 1);

		StopNetworkTest ();
		yield return StartCoroutine ("WaitNext", 2);

		ConnectTest ();
		yield return StartCoroutine ("WaitNext", 3);

		Test1 ();
		yield return StartCoroutine ("WaitNext", 4);

		//server disconnect
		RefusedByServerTest ();
		yield return StartCoroutine ("WaitNext", 5);

		ConnectTest ();
		yield return StartCoroutine ("WaitNext", 6);


		Test1();
		yield return StartCoroutine ("WaitNext", 7);

		ServerPushBackTest();
		yield return StartCoroutine ("WaitNext", 7);


//
//		Test2();
//		yield return StartCoroutine ("WaitNext");

		Debug.Log ("Test Complete !!");


	}


	IEnumerator WaitNext(int lineNumber) {
		int waitTime = Environment.TickCount + 3000;
		yield return new WaitUntil (() => {
			bool ret = nextEvent.WaitOne(0);
			if(!ret && waitTime < Environment.TickCount  ) {
				Debug.LogError("Time out !! Assert Failure: " + lineNumber);
			}

			return ret; 
		});
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI() {

//		int y = 10;
//		if (GUI.Button (new Rect (10, y, 300, 300), "Start Test")) {
//
//		}

	}

	void StopNetworkTest() {
		socketClient.OnDisconnect += (() => {
			socketClient.ResetAllEventForTest();
			nextEvent.Set();
		});
		
		socketClient.StopNetwork ();

	}

	void ConnectTest() {
		socketClient.OnConnect += (bool isConnect) => {
			Assert.IsTrue(isConnect);
			socketClient.ResetAllEventForTest();
			nextEvent.Set();
		};
		socketClient.StartNetwork ("127.0.0.1", 12000);
	}

	void ConnectFailTest() {
		socketClient.OnConnect += (bool isConnect) => {
			Assert.IsFalse(isConnect);
			socketClient.ResetAllEventForTest();
			nextEvent.Set();
		};
		socketClient.StartNetwork ("127.0.0.1", 10000);
	}

	void RefusedByServerTest() {
		socketClient.OnDisconnect += (() => {
			socketClient.ResetAllEventForTest();
			nextEvent.Set();
		});

			TestRequest req = new TestRequest ();
			req.type = 10;


			socketClient.Send (req, (object res) => {

			});

	}

	void ServerPushBackTest(){
		TestRequest req = new TestRequest ();
		req.type = 11;
		req.text = "test";
		req.floatArray = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };

		socketClient.OnReceiveServerPush += (int packetId, string protocolName, string response) => {
			Assert.IsTrue(packetId < 0);
			Assert.AreEqual(protocolName, "TEST");
			TestResponse res = JsonUtility.FromJson<TestResponse>(response);
			Assert.AreEqual(req.type , res.type);
			Debug.Log("response" + res.text);
			nextEvent.Set();
		};

		socketClient.Send (req, (object res) => {
			TestResponse response = (TestResponse) res;
			Assert.AreEqual(req.type , response.type);
			Assert.AreEqual(req.text , response.text);
			for (int i = 0 ; i < 4 ; i++) {
				Assert.AreEqual(req.floatArray[i] , response.floatArray[i]);
			}

		});

	}


	void Test1() {
			TestRequest req = new TestRequest ();
			req.type = 1;
			req.text = "test";
			req.floatArray = new float[] { 1.0f, 1.0f, 1.0f, 1.0f };


			socketClient.Send (req, (object res) => {
				TestResponse response = (TestResponse) res;
				Assert.AreEqual(req.type , response.type);
				Assert.AreEqual(req.text , response.text);
				for (int i = 0 ; i < 4 ; i++) {
					Assert.AreEqual(req.floatArray[i] , response.floatArray[i]);
				}
				nextEvent.Set();

			});
	}
	void Test2() {
			TestRequest req = new TestRequest ();
			req.type = 2;
			req.text = "test2";
			req.floatArray = new float[] { -0.5f, 1.0f, 1.0f, 1.0f };


			socketClient.Send (req, (object res) => {
				TestResponse response = (TestResponse) res;
				Assert.AreEqual(req.type , response.type);
				Assert.AreEqual(req.text , response.text);
				for (int i = 0 ; i < 4 ; i++) {
					Assert.AreEqual(req.floatArray[i] , response.floatArray[i]);
				}

			});
			nextEvent.Set();
	}
}
