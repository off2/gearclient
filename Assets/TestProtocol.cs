﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	Test Type (type )
	1 -10 : normal req-res
	11 : no response protocol 
 */
public class TestRequest {
	public int type;
	public float[] floatArray;
	public string text;
}

public class TestResponse: DefaultResponse {
	public int type;
	public float[] floatArray;
	public string text;
}


