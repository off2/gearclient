﻿using UnityEngine;
using UnityEngine.Assertions;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class ClientImpl : Singleton<ClientImpl>, IClient {

	private class  SendingJob {
		public object request;
		public ProtocolInfo protocolInfo;
		public ReceiveHandler receiveHandler;

	}

	private class  ReceivingJob {
		public int packetId;
		public long timeoutTick; 
		public ProtocolInfo protocolInfo;
		public ReceiveHandler receiveHandler;
	}

	private bool isConnect = false;
	private int receiveTimeoutInMilis = 0;

	private Thread sendingThread;
	private Thread receivingThread;
	private string url;
	private int port;
	private Socket socket;

	private Queue eventQueue;
	private Queue receiveQueue;

	private BlockingQueue<SendingJob> sendJobQueue;
	private SortedList receiveJobList;

	public event DisconnectHandler OnDisconnect;
	public event ReceiveServerPushHandler OnReceiveServerPush;
	public event ConnectHandler OnConnect;



	public ClientImpl() {
		sendJobQueue = new BlockingQueue<SendingJob> ();
		receiveJobList = SortedList.Synchronized (new SortedList ());


		eventQueue = Queue.Synchronized (new Queue ());
		receiveQueue = Queue.Synchronized (new Queue ());
	}


	public void StartNetwork(string url, int port) {

		if (isConnect)
			return;

		this.url = url;
		this.port = port;

		sendingThread = new Thread (new ThreadStart (SendingLoop));
		sendingThread.Start ();

	}

	public void StopNetwork (){
		if (!isConnect)
			return;

		socket.Close ();
		if (sendingThread != null)
			sendingThread.Interrupt ();
		sendingThread.Join();
		receivingThread.Join ();

	}

	private void ClearNetworkState() {
		isConnect = false;
		sendJobQueue.Clear ();
		receiveJobList.Clear ();

	}

	public void Send<REQ> (REQ request, ReceiveHandler receiveHandler){
		SendingJob job = new SendingJob ();
		job.request = request;
		job.receiveHandler = receiveHandler;
		job.protocolInfo = Protocols.getProtocol<REQ> ();

		sendJobQueue.Enqueue (job);

	}

	/*protocol 
		SIZE(4 bytes), PACKETID(4 bytes), NAME(10 bytes), DATA(?bytes json) 
		;SIZE including itself.

	*/

	public void SendingLoop() {
		IPAddress ipAddress = IPAddress.Parse (url);
        IPEndPoint remoteEP = new IPEndPoint(ipAddress,port);
		socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
		socket.Blocking = true;
		socket.SendTimeout = 0;


		//TODO socket option setting 
		//send timeout ,receive timeout

		try {
			socket.Connect (remoteEP);
		} catch (Exception e ) {
			Debug.Log ("" + e.ToString());
			QueueConnectEvent (false);
			return;
		}
		isConnect = true;
		receivingThread = new Thread (new ThreadStart (ReceivingLoop));
		receivingThread.Start ();

		QueueConnectEvent (true);

		int packetId = 0;
		while (true) {
			SendingJob job;
			job = sendJobQueue.WaitDequeue ();
			packetId++;
			if (job != null) {

				string json = JsonUtility.ToJson (job.request);
				string protocol = job.protocolInfo.Name.PadRight(10);
				int size = 8/* head + pakcetId*/ + json.Length + protocol.Length;
				byte[] sizeBytes =  BitConverter.GetBytes (size);
				byte[] packetIdBytes = BitConverter.GetBytes (packetId);
				if (BitConverter.IsLittleEndian) {
					Array.Reverse (sizeBytes);
					Array.Reverse (packetIdBytes);
				}
				byte[] buffer = new byte[size];
				Buffer.BlockCopy (sizeBytes, 0, buffer, 0, 4);
				Buffer.BlockCopy (packetIdBytes, 0, buffer, 4, 4);
				Buffer.BlockCopy (Encoding.ASCII.GetBytes (protocol), 0, buffer, 8, 10);
				Buffer.BlockCopy (Encoding.ASCII.GetBytes (json), 0, buffer, 18, json.Length);

				long jobListKey = 0;
				try {
					if(job.protocolInfo.HasResponse) {
						ReceivingJob receivingJob = new ReceivingJob();
						receivingJob.packetId = packetId;
						receivingJob.protocolInfo = job.protocolInfo;
						receivingJob.receiveHandler = job.receiveHandler;
						receivingJob.timeoutTick = job.protocolInfo.TimeOut * 10000000 + DateTime.Now.Ticks;
						receiveTimeoutInMilis = job.protocolInfo.TimeOut;

						Debug.Log("time tick: " + receivingJob.timeoutTick);

						receiveJobList.Add(receivingJob.timeoutTick, receivingJob);
						jobListKey = receivingJob.timeoutTick;


						//TODO signal to wakeup receiving socket;
					}

					int sentLength = socket.Send (buffer);
					//Blocking mode sent  
					Assert.AreEqual(size , sentLength);


				} catch (Exception e) { //should not happen except for close, So network colse operation is handled
										// receiving loop. 
					Debug.Log ("send socket error :" + e.ToString ());
					throw e;
				}

			}

		}

	}

	public void ReceivingLoop() {

		const int bufferSize = 2048;
		byte[] buffer = new byte[bufferSize];
		int offset = 0;
		int packetSize = int.MaxValue;
		while (true) {

			try {
				int readByte = socket.Receive(buffer, offset, bufferSize - offset, SocketFlags.None);
				if(readByte ==0)
					throw new Exception("socketEnd");
				offset += readByte;


				while(true) {
					if(offset >= 4) {
						packetSize = getIntFromBuffer(buffer, 0);
					}

					if(offset >= packetSize) {
						ProcessPacket(buffer, packetSize);
						offset -= packetSize;
						Buffer.BlockCopy(buffer, packetSize, buffer, 0, offset);
						packetSize = int.MaxValue;
					} else {
						break;
					}
				}

			} catch(Exception e) {
				Debug.Log ("receive socket error :" + e.ToString ());
				QueueDisconnetEvent ();
				sendingThread.Interrupt (); // quit 
				throw e;

			}
		}
	}

	private void ProcessPacket(byte[] buffer, int size) {
		int packetSize = getIntFromBuffer (buffer, 0);
		int packetId = getIntFromBuffer (buffer, 4);
		string protocolName = Encoding.ASCII.GetString (buffer, 8, 10).Trim('\0');
		string responseJson = Encoding.ASCII.GetString (buffer, 18, packetSize - 18);

		if (packetId < 0) {

			if (OnReceiveServerPush != null) {
				receiveQueue.Enqueue (new Action (delegate {
					OnReceiveServerPush(packetId, protocolName, responseJson);

				}));
			}
			return;
		}

		//TODO timeout 

		bool found = false;

		for (int idx = 0; idx < receiveJobList.Count; idx++) {
			ReceivingJob job = (ReceivingJob)receiveJobList.GetByIndex(idx);
			if (job.packetId == packetId) {
				found = true;
				Type type = job.protocolInfo.ResponseType;
				object responseObject = JsonUtility.FromJson (responseJson, type);


				receiveQueue.Enqueue (new Action (delegate {
					job.receiveHandler(responseObject);
				}));


				receiveJobList.RemoveAt (idx);
				break;
			}

		}

		if (!found)
			Assert.IsTrue (false);




	}

	private int getIntFromBuffer(byte[] src, int offset) {
		byte[] buffer = new byte[4];
		Buffer.BlockCopy (src, offset, buffer, 0, 4);
		if (BitConverter.IsLittleEndian)
			Array.Reverse (buffer);
		return BitConverter.ToInt32 (buffer, 0);
	}

	public void Update() {

		//receive data patch
		while(receiveQueue.Count > 0) {
			Action action = (Action) receiveQueue.Dequeue ();
			action ();
		}

		//event patch
		while(eventQueue.Count > 0) {
			Action action = (Action) eventQueue.Dequeue ();
			action ();
		}




	}

	private void QueueConnectEvent(bool connect) {
		if (OnConnect != null) {
			if (connect) {
				eventQueue.Enqueue (new Action (delegate {
					OnConnect (true);
				}));
			} else {
				eventQueue.Enqueue (new Action (delegate {
					OnConnect (false);
				}));
			}
		}
	}

	private void QueueDisconnetEvent() {
		if (OnDisconnect != null)
			eventQueue.Enqueue (new Action (delegate {
				ClearNetworkState();
				OnDisconnect();
			}));
	}


	public void ResetAllEventForTest() {
		OnDisconnect = null;
		OnConnect = null;
		OnReceiveServerPush = null;

	}

} 